package main

import (
	"testing"
	"fmt"
)

func TestRomanTranslate(t *testing.T) {
	result := KwegoToRoman("polsx polsx pol jin kil")
	if result != "MMXVI" {
		t.Error(fmt.Sprintf("Transalte to Roman was incorrect, got: %s, want: %s", result, "MMXIV"))
	}

}

func TestDecimalTranslate(t *testing.T) {
	result := RomanToDecimal("MMXVI")
	if result != 2016 {
		t.Error(fmt.Sprintf("Transalte to Decimal was incorrect, got: %d, want: %d", result, 2016))
	}

}