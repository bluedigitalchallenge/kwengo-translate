FROM golang:1.12.4

COPY bluedigital /go/src/bluedigital/
WORKDIR /go/src/bluedigital

ENTRYPOINT ["./bluedigital"]