package main

import (
	"fmt"
	 "os"
	 "strings"
)

func KwegoToRoman(k string) string {
	// Kwego Dict
	kwego := map[string]string {
		"kil": "I",
		"jin": "V",
		"pol": "X",
		"kilow": "L",
		"jij": "C",
		"jinjin": "D",
		"polsx": "M",
	}
	// Declarations
	fullStr := ""

	s := strings.Split(k, " ")
	for i := 0; i < len(s); i++ {
		if _, ok := kwego[s[i]]; ok {
			fullStr += kwego[s[i]]
		} else {
			fmt.Printf("The word '%v' is not present on kwego numbers vocabulary\n", s[i])
			os.Exit(1)
		}
	}

	return fullStr
}

func RomanToDecimal(r string) int {
	// Roman Dict
	roman := map[string]int {
		"I": 1,
		"V": 5,
		"X": 10,
		"L": 50,
		"C": 100,
		"D": 500,
		"M": 1000,
	}
	// Declarations
	var romanNums []string
	res := 0

	for i, c := range r {
		romanNums = append(romanNums, string(c))
		i++
	}

	j := 0
	
	for j < len(romanNums) {
		v1 := roman[romanNums[j]]
		if j+1 < len(romanNums) {
			v2 := roman[romanNums[j+1]]
			if v1 >= v2 {
				res += v1
				j++
			} else {
				res += v2 - v1
				j = j + 2
			}
		} else {
			res += v1
			j++
		}
	}

	return res
}

func main() {
	args := os.Args[1:]
	kwStr := ""
	romanStr := ""
	decimal := 0
	
	// Parameter check
	if len(args) == 0 {
		fmt.Println("Please pass a String Argument")
		os.Exit(1)
	} else if len(args) > 1 {
		fmt.Println("Please pass just one argument like 'polsx polsx pol jin kil'")
		os.Exit(2)
	} else {
		kwStr = args[0]
	}

	romanStr = KwegoToRoman(kwStr)

	fmt.Printf("Kwego Numbers => %v\n", kwStr)

	fmt.Printf("Roman Numbers => %v\n", romanStr)
	
	decimal = RomanToDecimal(romanStr)

	fmt.Printf("Decimal numbers => %v\n", decimal)
}