# Kwego Translation

## Description

Bluedigital challenge to solve the problem: "Translate kwego numbers to decimals"

## How to run

Binarie
```bash
go build
./bluedigital
```

Docker
```bash
docker build -t kwego-translate .
docker run kwego-translate "polsx polsx kil jin"
```

## Unit tests

On root directory
```bash
go test -v
```

## Try without code
```
docker run inyx/kwengo-translate "[KWENGO-STRING]"
```